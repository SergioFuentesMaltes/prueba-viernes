@runThis
Feature: Hacer Login en la web de Biwenger

  Background:
  Given el usuario accede a la web de biwenger del login
  When pulsa en ya tengo cuenta

#Scenario 1

  @Login @Correcto
  Scenario: El usuario navega a la website de Biwenger y hace el login correctamente

    And introduce usuario y contraseña correctamente
    And acepta el popup sobre privacidad
    Then el usuario puede ver su cuenta de usuario en biwenger


#Scenario 2
  @Login @Incorrecto
  Scenario: El usuario navega a la website de Biwenger y hace el login incorrectamente
    When introduce usuario correcto y contraseña incorrecta
    Then el usuario no puede acceder y aparece un mensaje de error en pantalla
