package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class AuthenticationSteps {
    private WebDriver driver;
    private ChromeOptions options = new ChromeOptions();


    @Given("^el usuario accede a la web de biwenger del login$")
    public void navigate_login_page_() throws Exception{
        System.out.println("Go_ to Login Page");
        System.setProperty("Webdriver.geckodriver", "./src/test/resources/drivers/chromedriver.exe");
        // Muy importante poner Chorme en modo HEADLESS
        options.setHeadless(true);

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        // Navegar a la página de Login
        driver.get("https://biwenger.as.com/login");
    }

    @When("^pulsa en ya tengo cuenta$")
    public void pulsar_ya_tengo_cuenta_() throws Exception{
        System.out.println("pulsamos en ya tengo cuenta");

        WebElement tengoCuentaBoton = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/p[1]/a"));
        tengoCuentaBoton.click();

    }
// Scenario 1:
    @When("^introduce usuario y contraseña correctamente$")
    public void introduce_datos_correctos_() throws Exception{
        System.out.println("Fill login form");

        WebElement userInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[1]"));
        WebElement passInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[2]"));
        userInput.clear();
        userInput.sendKeys("gerencia@maltessa.es");

        passInput.clear();
        passInput.sendKeys("Admin123");

        WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/button"));
        Thread.sleep(2000);
        searchButton.click();
        Thread.sleep(5000);
    }

    @When("^acepta el popup sobre privacidad$")
    public void acepta_privacidad_() throws Exception{
        System.out.println("Aceptamos popup sobre privacidad");


        WebElement aceptaPrivacidadBoton = driver.findElement(By.xpath("//*[@id=\"didomi-notice-agree-button\"]/span"));
        Wait<WebDriver> fwait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(2, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);


        aceptaPrivacidadBoton.click();

    }

    @Then("^el usuario puede ver su cuenta de usuario en biwenger$")
    public void user_sees_profile_() throws Exception {
        System.out.println("User sees profile");
        driver.quit();
    }
        //Scenario 2:

        @When("^introduce usuario correcto y contraseña incorrecta$")
        public void introduce_datos_incorrectos() throws Exception{
            System.out.println("Introducimos usuario correcto pero contraseña incorrecta");

            WebElement userInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[1]"));
            WebElement passInput = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/input[2]"));

            userInput.sendKeys("gerencia@maltessa.es");
            passInput.sendKeys("contraseñaerror");

            WebElement searchButton = driver.findElement(By.xpath("//*[@id=\"login-main\"]/div[2]/form/button"));
            Thread.sleep(5000);
            searchButton.click();
            Thread.sleep(5000);
        }

        @Then("^el usuario no puede acceder y aparece un mensaje de error en pantalla$")
        public void usuario_no_accede() throws Exception{
            System.out.println("Recibimos mensaje de error");

            WebElement mensajeError = driver.findElement(By.xpath("//*[@id=\"alert-container\"]/ng-component"));
            Assert.assertEquals("E-mail o contraseña no válidos.", mensajeError.getText());
            driver.quit();
        }



}
